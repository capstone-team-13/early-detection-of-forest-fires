# Early detection of forest fires

### [Capstone Website](http://web.cecs.pdx.edu/~faustm/capstone/index.php)

### [Google Drive link to documents here](https://drive.google.com/drive/folders/16-sOUmeAyqYsjJVhw6s7ufIG_nCUrHZA?usp=sharing)

This is our senior capstone project for Portland State University's Maseeh College of Engineering and Computer Science 2019:
A distributed alert system for early detection of forest fires.

Group members: Reem Abdo (abdoreem@pdx.edu), Samantha Fink (scleary@pdx.edu), Brianne Park (bpark@pdx.edu), and Martín Rodriguez (mtr@pdx.edu)

Faculty advisor: Dr. John Lipor (lipor@pdx.edu)

Industry sponsor: Karthik Natarajan (kartik.natarajan@intel.com)
