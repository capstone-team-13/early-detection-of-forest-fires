
Shinyei PPD42NS particle sensor
pin 2 must have a voltage divider to reduce 5v to 3.3v for Pi

- pin + -> 5v
- pin 2 -> GPIO 07
- pin - -> GND
- 



## Accounts
You'll need a thingspeak account, and to create a channel with three fields, and get your API key
You'll need a slack account, and to get your API key


## Installation
1. Starting with Raspbian Jessie Lite, update the system:

``` bash
sudo apt-get update
sudo apt-get install python-dev python-pip git
``` 

2. Enable i2c by: sudo raspi-config and Interface Options > I2C > Enable
And whilst you're in the config program, change your timezone
Exit and reboot

3. Clone this repo and the BMP085 repo:
``` bash
git clone https://github.com/andy-pi/weather-monitor.git
git clone https://github.com/adafruit/Adafruit_Python_BMP.git
cd Adafruit_Python_BMP
sudo python setup.py install
```

4. Install PIGPIO, a library for low level GPIO operations (this repo includes DHT22.py)
(http://abyz.co.uk/rpi/pigpio/)
``` bash
wget abyz.co.uk/rpi/pigpio/pigpio.zip
unzip pigpio.zip
cd PIGPIO
make
sudo make install
```

# Air Quality - Shinyei PPD42NS only

## Basic Printout of air quality

Carry out steps 1-4 above and then:  

```bash
sudo pigpiod
python air_quality.py
```

The output will be printed on screen. Note that the instantaneous AQI is not actually correct, it should be taken as a 24 average of the PM2.5 particles in micrograms/m3