
import pandas as pd

import numpy as np
import datetime
import csv
import chardet #used to detect characters; needed for Python Notebook, may not need in file?
import time


while True:
	df = pd.read_csv('PM_data.csv')
	names = ['Timestamp','PM Value(ug/m^3)', 'Sum'] #this line causes error in notebook
	df['Timestamp'] = pd.to_datetime(df['Timestamp']) 
	recent_dateP = df['Timestamp'].max() #find most recent date and store value
	p_sensor_sum =(df.loc[df['Timestamp'] == recent_dateP]['Sum'].values) #find summing value for latest time stamp
	pSensorInt = int(p_sensor_sum) #change value to integer

	print("PM SUM: ", pSensorInt) #print summing value for latest time stamp
	print("PM TIMESTAMP: ", recent_dateP)
	print("")

	#read from temp sensor file
	df = pd.read_csv('temp_data.csv')
	names = ['Timestamp','Temperature (degrees C)', 'Sum'] #this line causes error in notebook
	df['Timestamp'] = pd.to_datetime(df['Timestamp']) 
	recent_dateT = df['Timestamp'].max() #find most recent date and store value
	t_sensor_sum =(df.loc[df['Timestamp'] == recent_dateT]['Sum'].values) #find summing value for latest time stamp
	tSensorInt = int(t_sensor_sum)

	print("TEMP SUM: ", tSensorInt) #print summing value for latest time stamp
	print("TEMP TIMESTAMP: ", recent_dateT)
	print("")

	#read from humidity sensor file
	df = pd.read_csv('hum_data.csv')
	names = ['Timestamp','Humidity Value (%Relavtive)', 'Sum'] #this line causes error in notebook
	df['Timestamp'] = pd.to_datetime(df['Timestamp']) 
	recent_dateH = df['Timestamp'].max() #find most recent date and store value
	h_sensor_sum =(df.loc[df['Timestamp'] == recent_dateT]['Sum'].values) #find summing value for latest time stamp
	hSensorInt = int(t_sensor_sum)

	print("HUMIDITY SUM: ", hSensorInt) #print summing value for latest time stamp
	print("HUMIDITY TIMESTAMP: ", recent_dateH)
	print("")

	#read from camera data file
	df = pd.read_csv('/home/pi/early-detection-of-forest-fires/fd-demo/camera_data.csv')
	names = ['Timestamp','Camera Certainty', 'Sum'] #this line causes error in notebook
	df['Timestamp'] = pd.to_datetime(df['Timestamp']) 
	recent_dateC = df['Timestamp'].max() #find most recent date and store value
	c_sensor_sum =(df.loc[df['Timestamp'] == recent_dateC]['Sum'].values) #find summing value for latest time stamp
	cSensorInt = int(c_sensor_sum)

	print("CAMERA SUM: ", cSensorInt) #print summing value for latest time stamp
	print("CAMERA TIMESTAMP: ", recent_dateC)
	print("")



	weightArray = [4, 3, 2, 1]

	sumArray = [cSensorInt,tSensorInt,hSensorInt,pSensorInt]
	alertArray = np.dot(weightArray, sumArray)

	print("Sum Array:")
	print(sumArray)

	print("Weight Array: ")
	print(weightArray)


	print("Alert Array:")
	print(alertArray)

	time.sleep(60) #sleep for 60 seconds











# open file
# select row with largest value/latest timestamp
# take value from 3rd column 
# add above value to summing program 
# repeat with next file
# ...
# once all values obtained, add.
# check added value against specific ranges
# determine if alert or not
# if alert: send alert and reset alert value
# else reset alert value
# repeat