# -*- coding: utf-8 -*-
'''
Original from http://abyz.co.uk/rpi/pigpio/examples.html
'''
# Written by David Neuy
# Version 0.1.0 @ 03.12.2014
# This script was first published at: http://www.home-automation-community.com/
# You may republish it as is or publish a modified version only when you 
# provide a link to 'http://www.home-automation-community.com/'. 


from __future__ import print_function
import math
import pigpio
import pandas as pd
import datetime
import csv
import chardet  

#install dependency with 'sudo easy_install apscheduler' NOT with 'sudo pip install apscheduler'
import os, sys, Adafruit_DHT, time
from datetime import datetime, date
from apscheduler.schedulers.background import BackgroundScheduler

class sensor:
   """
   A class to read a Shinyei PPD42NS Dust Sensor, e.g. as used
   in the Grove dust sensor.

   This code calculates the percentage of low pulse time and
   calibrated concentration in particles per 1/100th of a cubic
   foot at user chosen intervals.

   You need to use a voltage divider to cut the sensor output
   voltage to a Pi safe 3.3V (alternatively use an in-line
   20k resistor to limit the current at your own risk).
   """

   def __init__(self, pi, gpio):
      """
      Instantiate with the Pi and gpio to which the sensor
      is connected.
      """

      self.pi = pi
      self.gpio = gpio

      self._start_tick = None
      self._last_tick = None
      self._low_ticks = 0
      self._high_ticks = 0

      pi.set_mode(gpio, pigpio.INPUT)

      self._cb = pi.callback(gpio, pigpio.EITHER_EDGE, self._cbf)

   def read(self):
      """
      Calculates the percentage low pulse time and calibrated
      concentration in particles per 1/100th of a cubic foot
      since the last read.

      For proper calibration readings should be made over
      30 second intervals.

      Returns a tuple of gpio, percentage, and concentration.
      """
      interval = self._low_ticks + self._high_ticks

      if interval > 0:
         ratio = float(self._low_ticks)/float(interval)*100.0
         conc = 1.1*pow(ratio,3)-3.8*pow(ratio,2)+520*ratio+0.62;
      else:
         ratio = 0
         conc = 0.0

      self._start_tick = None
      self._last_tick = None
      self._low_ticks = 0
      self._high_ticks = 0

      return (self.gpio, ratio, conc)

   def _cbf(self, gpio, level, tick):

      if self._start_tick is not None:

         ticks = pigpio.tickDiff(self._last_tick, tick)

         self._last_tick = tick

         if level == 0: # Falling edge.
            self._high_ticks = self._high_ticks + ticks

         elif level == 1: # Rising edge.
            self._low_ticks = self._low_ticks + ticks

         else: # timeout level, not used
            pass

      else:
         self._start_tick = tick
         self._last_tick = tick
         

   def pcs_to_ugm3(self, concentration_pcf):
        '''
        Convert concentration of PM2.5 particles per 0.01 cubic feet to µg/ metre cubed
        this method outlined by Drexel University students (2009) and is an approximation
        does not contain correction factors for humidity and rain
        '''
        
        if concentration_pcf < 0:
           raise ValueError('Concentration cannot be a negative number')
        
        # Assume all particles are spherical, with a density of 1.65E12 µg/m3
        densitypm25 = 1.65 * math.pow(10, 12)
        
        # Assume the radius of a particle in the PM2.5 channel is .44 µm
        rpm25 = 0.44 * math.pow(10, -6)
        
        # Volume of a sphere = 4/3 * pi * radius^3
        volpm25 = (4/3) * math.pi * (rpm25**3)
        
        # mass = density * volume
        masspm25 = densitypm25 * volpm25
        
        # parts/m3 =  parts/foot3 * 3531.5
        # µg/m3 = parts/m3 * mass in µg
        concentration_ugm3 = concentration_pcf * 3531.5 * masspm25
        
        return concentration_ugm3


   def ugm3_to_aqi(self, ugm3):
        '''
        Convert concentration of PM2.5 particles in µg/ metre cubed to the USA 
        Environment Agency Air Quality Index - AQI
        https://en.wikipedia.org/wiki/Air_quality_index
	Computing_the_AQI
        https://github.com/intel-iot-devkit/upm/pull/409/commits/ad31559281bb5522511b26309a1ee73cd1fe208a?diff=split
        '''
        
        cbreakpointspm25 = [ [0.0, 12, 0, 50],\
                        [12.1, 35.4, 51, 100],\
                        [35.5, 55.4, 101, 150],\
                        [55.5, 150.4, 151, 200],\
                        [150.5, 250.4, 201, 300],\
                        [250.5, 350.4, 301, 400],\
                        [350.5, 500.4, 401, 500], ]
                        
        C=ugm3
        
        if C > 500.4:
            aqi=500

        else:
           for breakpoint in cbreakpointspm25:
               if breakpoint[0] <= C <= breakpoint[1]:
                   Clow = breakpoint[0]
                   Chigh = breakpoint[1]
                   Ilow = breakpoint[2]
                   Ihigh = breakpoint[3]
                   aqi=(((Ihigh-Ilow)/(Chigh-Clow))*(C-Clow))+Ilow
        
        return aqi
       

if __name__ == "__main__":

   import time

   while True:
      pi = pigpio.pi() # Connect to Pi.
   
      s = sensor(pi, 7) # set the GPIO pin number

      # Use 30s for a properly calibrated reading.
      time.sleep(30) 
      
      # get the gpio, ratio and concentration in particles / 0.01 ft3
      g, r, c = s.read()

      if (c==1114000.62):
          print("Error\n")
          continue

      print("Air Quality Measurements for PM2.5:")
      print("  " + str(int(c)) + " particles/0.01ft^3")

      # convert to SI units
      concentration_ugm3=s.pcs_to_ugm3(c)
      print("  " + str(int(concentration_ugm3)) + " ugm^3")
      
      # convert SI units to US AQI
      # input should be 24 hour average of ugm3, not instantaneous reading
      aqi=s.ugm3_to_aqi(concentration_ugm3)
      
      print("  Current AQI (not 24 hour avg): " + str(int(aqi)))
      print("")

      #determine summing value
      if concentration_ugm3 > 10:
      	pSum = 1
      else: 
      	pSum = 0

      current_time =(pd.datetime.now().strftime("%m-%d-%Y %H:%M:%S"))
      pSensor_data = {'Time Stamp': [current_time], 
      'Particulate Value': [round(concentration_ugm3,2)],
      'Sum': [pSum]}
      df = pd.DataFrame(pSensor_data, columns = ['Time Stamp','Particulate Value', 'Sum'])
   
   # write data to .csv file
      with open('PM_data.csv','a') as f:
      	df.to_csv(f, header=False, index=False)
      print("Time written to file: ", current_time)
      print("PM data written to file: ", round(concentration_ugm3,2))
      print("PM Sum written to file: ", pSum)
      print("")

      pi.stop()
      
##TEMP + HUMIDITY START
#sensor                       = Adafruit_DHT.AM2302 #DHT11/DHT22/AM2302
      tSensor                       = Adafruit_DHT.DHT22 #DHT11/DHT22/AM2302
      pin                          = 4
      sensor_name                  = "living-room"
      hist_temperature_file_path   = "sensor-values/temperature_" + sensor_name + "_log_" + str(date.today().year) + ".csv"
      latest_temperature_file_path = "sensor-values/temperature_" + sensor_name + "_latest_value.csv"
      hist_humidity_file_path      = "sensor-values/humidity_" + sensor_name + "_log_" + str(date.today().year) + ".csv"
      latest_humidity_file_path    = "sensor-values/humidity_" + sensor_name + "_latest_value.csv"
      csv_header_temperature       = "timestamp,temperature_in_celsius\n"
      csv_header_humidity          = "timestamp,relative_humidity\n"
      csv_entry_format             = "{:%Y-%m-%d %H:%M:%S},{:0.1f}\n"
      sec_between_log_entries      = 60
      latest_humidity              = 0.0
      latest_temperature           = 0.0
      latest_value_datetime        = None

      def write_header(file_handle, csv_header):
        file_handle.write(csv_header)

      def write_value(file_handle, datetime, value):
          line = csv_entry_format.format(datetime, value)
          file_handle.write(line)
          file_handle.flush()

      def open_file_ensure_header(file_path, mode, csv_header):
        f = open(file_path, mode, os.O_NONBLOCK)
        if os.path.getsize(file_path) <= 0:
          write_header(f, csv_header)
        return f

      def write_hist_value_callback():
        write_value(f_hist_temp, latest_value_datetime, latest_temperature)
        write_value(f_hist_hum, latest_value_datetime, latest_humidity)

      def write_latest_value():
        with open_file_ensure_header(latest_temperature_file_path, 'w', csv_header_temperature) as f_latest_value:  #open and truncate
          write_value(f_latest_value, latest_value_datetime, latest_temperature)
        with open_file_ensure_header(latest_humidity_file_path, 'w', csv_header_humidity) as f_latest_value:  #open and truncate
          write_value(f_latest_value, latest_value_datetime, latest_humidity)

      f_hist_temp = open_file_ensure_header(hist_temperature_file_path, 'a', csv_header_temperature)
      f_hist_hum  = open_file_ensure_header(hist_humidity_file_path, 'a', csv_header_humidity)


      for x in range(2):
        Adafruit_DHT.read_retry(tSensor, pin)


#create timer that is called every n seconds, without accumulating delays as when using sleep
      scheduler = BackgroundScheduler()
      scheduler.add_job(write_hist_value_callback, 'interval', seconds=sec_between_log_entries)
      scheduler.start()
#print "Started interval timer which will be called the first time in {0} seconds.".format(sec_between_log_entries);

#try:
#while True:
      hum, temp = Adafruit_DHT.read_retry(tSensor, pin)
      if hum is not None and temp is not None:
        if temp < -40.0 or temp > 80.0:
          print("blah")
        else:
          latest_temperature = temp
       

        if hum < 0.0 or hum > 100.0:
          print("blah blah")
        else:
          latest_humidity = hum
          latest_value_datetime = datetime.today()
          write_latest_value()
     

#determine summing value for temp sensor
      if temp > 26:
        tSum = 1
      else: 
        tSum = 0

#determine summing value for humidity sensor
      if hum < 10:
        hSum = 1
      else:
        hSum = 0

      tempInt = round(temp,2)
      humInt = round(hum,2)

# write values to csv data
      current_time =(pd.datetime.now().strftime("%m-%d-%Y %H:%M:%S"))
      tSensor_data = {'Time Stamp': [current_time],
      'Temperature': [tempInt],
      'Sum': [tSum]}
      df = pd.DataFrame(tSensor_data, columns = ['Time Stamp','Temperature', 'Sum'])

      with open('temp_data.csv','a') as g:
        df.to_csv(g, header=False, index=False)
      print("Temp write time: ", current_time)
      print("Temp written to file: ", tempInt)
      print("Temp sum: ", tSum)
      print("")
      
      pi.stop()

      hSensor_data = {'Time Stamp': [current_time],
      'Humidity': [humInt],
      'Sum': [hSum]}
      df = pd.DataFrame(hSensor_data, columns = ['Time Stamp','Humidity', 'Sum'])

      with open('hum_data.csv','a') as f:
        df.to_csv(f,header=False, index=False)
      print("Humidity write time: ",current_time)
      print("Humidity written to file: ", humInt)
      print("Humidity sum: ", hSum)
      print("")
      pi.stop()
          



#except (KeyboardInterrupt, SystemExit):
  #scheduler.shutdown()
      #break



