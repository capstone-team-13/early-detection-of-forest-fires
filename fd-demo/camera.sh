#!/bin/bash

DATE=$(date +"%Y-%m-%d_%H%M")

raspistill -md 5 -w 1280 -h 720 -tl 1000 -t 10000 -o /home/pi/fd-demo/img_%04d.jpg
