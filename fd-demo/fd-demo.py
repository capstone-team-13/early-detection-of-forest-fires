#!/usr/bin/env python3
#
# Copyright 2017 Google Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
"""
Script to run retrained fire detection MobileNet based classification model.
    // Current Features //
        - Input from PiCamera
        - Console prints output, either fire detected or no_fire detected, with the likelihood
        of the label being true (`sudo journalctl -u fd-demo -f` must be entered if running as service )
        - Video with overlay is streamed to 10.0.0.52:4664 on local network
        - Alarm sound plays and urwid alert output is printed to console when fire is first detected, and when it is detected again after 15 seconds
        - Runs indefinitely as service on boot, exits on CTRL-C (SIGINT) - for debugging service is disabled.
    // Planned Features //
        - Integrate particulate sensors to make decision about presence of fire
        - Network connectivity via LoRa wireless network
            - Unit will transmit alert to next station until internet connectivity is detected, then send alert to fire professionals
"""

import argparse # argparse makes working with  program arguments easier
try:
    import httplib
except:
    import http.client as httplib
import logging # logging allows for logging and debug messages to be saved
import signal # signal is used to capture SIGINT and SIGTERM signals from os > stop program
import socket
import string
import sys
import time
import urwid

from logging.handlers import TimedRotatingFileHandler, HTTPHandler, DatagramHandler
from picamera import PiCamera, Color

from aiy.toneplayer import TonePlayer
from aiy.vision import inference
from aiy.vision.models import utils
from aiy.vision.streaming.server import StreamingServer
from aiy.vision.streaming import svg

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)

ALARM_SOUND = ('f5h', 'D5h', 'f5h', 'D5h', 'f5h', 'D5h')
WE_DIDNT_START_THE_FIRE = ('G3w', 'rw', 'G3w', 'rw', 'B4h', 'rh', 'B4h', 'rh', 'C4h', 'rw', 'A6w', 'rw', 'B6w')
FONT_FILE = '/usr/share/fonts/truetype/freefont/FreeSans.ttf'
BUZZER_GPIO = 22

count = 0   # count keeps track of how many times 'fire' status is printed to log

def create_timed_rotating_log(log_path):
    """ Create a timed, rotating file handler to rollover at midnight, keeping 90 files on disk """
    debug_handler = TimedRotatingFileHandler(log_path, when='midnight', 
                                             interval=1, backupCount= 90, 
                                             encoding=None, delay=False, utc=False)
    debug_handler.setLevel(logging.DEBUG)
    
    # Create a logging format
    debug_formatter = logging.Formatter('%(asctime)s %(name)s %(levelname)s %(message)s')
    debug_handler.setFormatter(debug_formatter)

    # Add handlers to logger
    logger.addHandler(debug_handler)
    
    """ Uncomment the line below when ready for production 
    (Allows user to CTRL-C to exit without error shown) """
    # logging.raiseExceptions = False

def create_datagram_log():
    """ Create a datagram log handler to send alert via UDP when no web connectivity is detected """
    host = 'pi@fd-unit-01' # Need to search for host to find actual hostname
    port = '' # TBD
    udp_handler = DatagramHandler(host, port)

    logger.addHandler(udp_handler)
    udp_handler.setLevel(logging.INFO)

def create_http_log():
    """ Create a HTTP log handler to send alert to web server. Log is sent in dict format """
    server = '127.0.0.1:5000' # placeholder for actual web server
    path = '/'
    method = 'GET'
    http_handler = HTTPHandler(server, path, method=method)

    logger.addHandler(http_handler)
    http_handler.setLevel(logging.INFO)

def check_internet():
    conn = httplib.HTTPConnection("www.google.com", timeout=5)
    try:
        conn.request("HEAD", "/")
        conn.close()
        return True
    except:
        conn.close()
        return False

def read_labels(label_path):
    """Reads labels and formats them for model"""
    with open(label_path) as label_file:
        return [label.strip() for label in label_file.readlines()]

def get_message(result, threshold, top_k):
    """Generates message for fire/no fire/nothing detected scenarios"""
    if result:
        return 'Detecting:\n %s' % '\n'.join(result)
    return 'Nothing detected when threshold=%.2f, top_k=%d' % (threshold, top_k)

def svg_overlay(message):
    """Generates an overlay for streaming and for image capture"""
    """TODO: Not sure if this currently works for image capture, or if we should even include that functionality"""
    width = 2
    height = 5
    doc = svg.Svg(width=width, height=height)
    doc.add(svg.Text(message, x=10, y=10,
                     fill='red', font_size=30))
    return str(doc) 

def capture_image(camera):
    """Captures an image in the event of a fire detection and saves to disk"""
    timestr = time.strftime("%m%d%y-%H%M%S")
    capture_filepath = '/home/pi/early-detection-of-forest-fires/fd-demo/captures/' + timestr + '.jpg'
    camera.capture(capture_filepath)
    logger.info('Image captured at: %s' % capture_filepath)
    return 0

class Alert:

    def __init__(self):
        self.alert_palette = [
            ('banner', '', '', '', '#ffa', '#60d'),
            ('streak', '', '', '', 'g50', '#60a'),
            ('inside', '', '', '', 'g38', '#808'),
            ('outside', '', '', '', 'g27', '#a06'),
            ('bg', '', '', '', 'g7', '#d06'),]
        self.placeholder = urwid.SolidFill()
        self.loop = urwid.MainLoop(self.placeholder, self.alert_palette, unhandled_input=self.exit_on_q)
        self.loop.screen.set_terminal_properties(colors=256)
        self.loop.set_alarm_in(10, self.exit_on_alarm)
        self.loop.widget = urwid.AttrMap(self.placeholder, 'bg')
        self.loop.widget.original_widget = urwid.Filler(urwid.Pile([]))
        self.div = urwid.Divider()
        self.outside = urwid.AttrMap(self.div, 'outside')
        self.inside = urwid.AttrMap(self.div, 'inside')
        self.alert_txt = urwid.Text(('banner', u" ALERT: FIRE DETECTED "), align='center')
        self.streak = urwid.AttrMap(self.alert_txt, 'streak')
        self.pile = self.loop.widget.base_widget # .base_widget skips the decorations
        for item in [self.outside, self.inside, self.streak, self.inside, self.outside]:
            self.pile.contents.append((item, self.pile.options()))
    
    def __enter__(self):
        return self

    def __exit__(self, exception_type, exception_value, traceback):
        pass
        
    def exit_on_q(self, key):
        logger.debug('Key pressed. Closing urwid alert.')
        if key in ('q', 'Q'):
            raise urwid.ExitMainLoop()

    def exit_on_alarm(self, loop=None, data=None):
        logger.debug('Alarm time elapsed. Closing urwid alert.')
        raise urwid.ExitMainLoop()

    def urwid_alert(self):
        logger.debug('Now displaying urwid alert in console.')
        self.loop.run()

def process(result, labels, tensor_name, threshold, top_k):
    """Processes inference result and returns labels sorted by confidence."""
    # MobileNet based classification model returns one result vector
    assert len(result.tensors) == 1
    tensor = result.tensors[tensor_name]
    probs, shape = tensor.data, tensor.shape
    assert shape.depth == len(labels)
    pairs = [pair for pair in enumerate(probs) if pair[1] > threshold]
    pairs = sorted(pairs, key=lambda pair: pair[1], reverse=True)
    pairs = pairs[0:top_k]
    return [' %s (%.2f)' % (labels[index], prob) for index, prob in pairs]

def fire_detector(model, labels, output_layer, threshold, top_k, num_frames, preview, 
                  show_fps, refresh_rate, 
                  enable_streaming, streaming_bitrate, mdns_name):
    """Handles fire detection via machine vision - retrained mobileNet model"""
    def stop():
        logger.info('Stopping...')
        print('Stopping fire detection demo.')
        if preview:
            camera.stop_preview()
        if server:
            server.close()
        return 0

    signal.signal(signal.SIGINT, lambda signum, frame: stop())
    signal.signal(signal.SIGTERM, lambda signum, frame: stop())

    logger.info('Starting...')
    with PiCamera(sensor_mode=4, resolution=(820, 616), framerate=30) as camera: # Works with 1640, 1232
        
        if preview:
            camera.start_preview()
        
        server = None

        if enable_streaming:
            server = StreamingServer(camera, bitrate=streaming_bitrate, 
                                     mdns_name=mdns_name)

        fire_flag = ""
        count = 0

        with inference.CameraInference(model) as camera_inference:
            for result in camera_inference.run(num_frames):
                processed_result = process(result, labels, output_layer,
                                           threshold, top_k)
                # fire_flag is either 'fire' or 'no_fire'. 
                # Output 'fire' to log is limited to once every 60 seconds, to avoid oversaturating log
                fire_flag = str(processed_result)
                fire_flag = "".join(c for c in fire_flag if c not in " '[]().0123456789")    
                if fire_flag == "fire":
                    if count == 0:  # fire_flag has not been set before, output should go to log    
                        internet_status = check_internet()
                        if internet_status == True:
                            # create_http_log()
                            logger.info('Internet connection detected. Sending log via HTTP socket to web server.')
                        else:
                            # create_datagram_log()
                            logger.info('No internet connected detected. Sending log via UDP socket to next available unit.')
                        """TODO: change str to get hostname dynamically from pi"""
                        logger.info('Fire detected near fd-unit-02')     
                        capture_image(camera)
                        player = TonePlayer(gpio=BUZZER_GPIO, bpm=10)
                        player.play(*ALARM_SOUND)
                        with Alert() as alert:
                            alert.urwid_alert()
                        count = count + 1
                        start_time = time.monotonic()
                    elif (time.monotonic() - start_time > 15.0): # 15 seconds since last detected fire, output should go to log
                        internet_status = check_internet()
                        if internet_status == True:
                            logger.info('Internet connection detected. Sending log via HTTP socket to web server.')
                            # create_http_log()
                        else:
                            logger.info('No internet connected detected. Sending log via UDP socket to next available unit.')
                            # create_datagram_log()
                        """TODO: change str to get hostname dynamically from pi"""
                        logger.info('Fire detected near fd-unit-02')
                        capture_image(camera)
                        player = TonePlayer(gpio=BUZZER_GPIO, bpm=10)
                        player.play(*ALARM_SOUND)
                        with Alert() as alert:
                            alert.urwid_alert()
                        count = count + 1
                        start_time = time.monotonic()
                message = get_message(processed_result, threshold, top_k)
                if show_fps:
                    message += '\nWith %.1f FPS.' % camera_inference.rate
                if refresh_rate != 0.0:
                    time.sleep(refresh_rate)
                print(message)

                if preview:
                    camera.annotate_foreground = Color('black')
                    camera.annotate_background = Color('white')
                    # PiCamera text annotation only supports ascii.
                    camera.annotate_text = '\n %s' % message.encode(
                        'ascii', 'backslashreplace').decode('ascii')
                
                if server:
                    server.send_overlay(svg_overlay(message))

        if preview:
            camera.stop_preview()

        if server:
            server.close()

def main():
    # Set logging file path and file name and create log
    log_path = '/home/pi/early-detection-of-forest-fires/fd-demo/logs/logtest.log'
    create_timed_rotating_log(log_path)
    player = TonePlayer(gpio=BUZZER_GPIO, bpm=10)
    player.play(*WE_DIDNT_START_THE_FIRE)

    parser = argparse.ArgumentParser()
    parser.add_argument('--model_path', required=True,
                        help='Path to converted model file that can run on VisionKit.')
    parser.add_argument('--label_path', required=True,
                        help='Path to label file that corresponds to the model.')
    parser.add_argument('--input_height', type=int, required=True, 
                        help='Input height.')
    parser.add_argument('--input_width', type=int, required=True, 
                        help='Input width.')
    parser.add_argument('--input_layer', required=True, 
                        help='Name of input layer.')
    parser.add_argument('--output_layer', required=True, 
                        help='Name of output layer.')
    parser.add_argument('--num_frames', type=int, default=None,
                        help='Sets the number of frames to run for, otherwise runs forever.')
    parser.add_argument('--input_mean', type=float, default=128.0, 
                        help='Input mean.')
    parser.add_argument('--input_std', type=float, default=128.0, 
                        help='Input std.')
    parser.add_argument('--input_depth', type=int, default=3, 
                        help='Input depth.')
    parser.add_argument('--threshold', type=float, default=0.1,
                        help='Threshold for classification score (from output tensor).')
    parser.add_argument('--top_k', type=int, default=3, 
                        help='Keep at most top_k labels.')
    parser.add_argument('--preview', action='store_true', default=False,
                        help='Enables camera preview in addition to printing result to terminal.')
    parser.add_argument('--show_fps', action='store_true', default=False,
                        help='Shows end to end FPS.')
    parser.add_argument('--refresh_rate', type=float, default=0.0,
                        help='Specify a refresh rate. Affects preview, stream, and printing to terminal.')
    parser.add_argument('--enable_streaming', default=False, action='store_true',      
                        help='Enable streaming server.')
    parser.add_argument('--streaming_bitrate', type=int, default=1000000,
                        help='Streaming server video bitrate (kbps)')
    parser.add_argument('--mdns_name', default='',
                        help='Streaming server mDNS name')
    args = parser.parse_args()

    input_height = args.input_height
    input_width = args.input_width
    input_depth = args.input_depth
    input_mean = args.input_mean
    input_std = args.input_std
    model_path = args.model_path
    label_path = args.label_path
    output_layer = args.output_layer
    threshold = args.threshold
    top_k = args.top_k

    model = inference.ModelDescriptor(
        name='mobilenet_based_classifier_fire_detection',
        input_shape=(1, input_height, input_width, input_depth),
        input_normalizer=(input_mean, input_std),
        compute_graph=utils.load_compute_graph(model_path))
    labels = read_labels(label_path)
    refresh_rate = args.refresh_rate
    enable_streaming = args.enable_streaming
    streaming_bitrate = args.streaming_bitrate
    mdns_name = args.mdns_name
    num_frames = args.num_frames
    preview = args.preview
    show_fps = args.show_fps

    try:
        fire_detector(model, labels, output_layer, threshold, top_k, num_frames,
                      preview, show_fps, refresh_rate, 
                      enable_streaming, streaming_bitrate, mdns_name)
    except KeyboardInterrupt:
        pass
    except Exception:
        logger.exception('Exception while running fire detection demo.')
    
    return 0

if __name__ == '__main__':
    main()
