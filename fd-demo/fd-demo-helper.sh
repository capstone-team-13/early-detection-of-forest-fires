#!/usr/bin/env bash

/home/pi/early-detection-of-forest-fires/fd-demo/fd-demo.py \
	--model_path /home/pi/early-detection-of-forest-fires/fd-demo/tf_files/fire_detection.binaryproto \
	--label_path /home/pi/early-detection-of-forest-fires/fd-demo/tf_files/fire_detection_labels.txt \
	--input_height 160 \
	--input_width 160 \
	--input_layer input \
	--output_layer final_result \
	--threshold 0.7 \
	--preview \
	--show_fps \
	--enable_streaming \
	--mdns_name "Vision-Bonnet-3397"
